function assoc(k, v, obj) {
    if (Array.isArray(obj)) {
        const res = obj.slice();
        res[k] = v;
        return res;
    }
    return { ...obj, [k]: v };
}

function assocIn(path, v, obj) {
    if (!path.length)
        return v;
    const k = path[0];
    return assoc(k, assocIn(path.slice(1), v, obj[k]), obj);
}

function getIn(path, obj) {
    let res = obj;
    path.forEach(function (k) {
        res = res[k];
    });
    return res;
}

module.exports.getIn = getIn;

module.exports.assoc = assoc;

module.exports.assocIn = assocIn;