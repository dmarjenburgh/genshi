function swap(arr, i, j) {
    const t = arr[i];
    arr[i] = arr[j];
    arr[j] = t;
}

function swapUp(arr, cmp, i) {
    let pIndex = Math.floor(i / 2);
    while (pIndex) {
        if (cmp(arr[i], arr[pIndex]) < 0) {
            swap(arr, i, pIndex);
            i = pIndex;
            pIndex = Math.floor(i / 2);
        } else
            break;
    }
}

function swapDown(arr, cmp, idx) {
    let li, ri, lc, rc, ci;
    do {
        li = idx * 2;
        ri = li + 1;
        lc = arr[li];
        rc = arr[ri];
        if (rc && cmp(rc, arr[idx]) < 0) {
            ci = cmp(lc, rc) < 0 ? li : li + 1;
        } else {
            if (lc && cmp(lc, arr[idx]) < 0)
                ci = li;
            else
                break;
        }
        swap(arr, idx, ci);
        idx = ci;
    } while (lc);
}

function defaultComparator(a, b) {
    return a - b;
}

const Heap = function (compareFn) {
    this._arr = new Array(1);
    this._compareFn = compareFn || defaultComparator;
    this._index = 0;
};
Heap.prototype.push = function (x) {
    const arr = this._arr;
    arr[++this._index] = x;
    swapUp(arr, this._compareFn, this._index);
    return this._index;
};

Heap.prototype.pop = function () {
    if (!this._index)
        return;

    const arr = this._arr;
    const x = arr[1];
    arr[1] = arr[this._index];
    arr[this._index--] = undefined;
    swapDown(arr, this._compareFn, 1);
    return x;
};

Heap.prototype.size = function () {
    return this._index;
};

module.exports = Heap;