const Heap = require("./heap");
const fp = require("./fp");

const cellRankComparator = function (c1, c2) {
    return c1._rank - c2._rank;
};

// Propagation algorithm 1 - Using heap
const invalidateNode = function (node, stack) {
    node._visitCount = 0;
    if (node.invalidate()) {
        stack.push.apply(stack, node._sinks);
    }
};

const visit = function (node, stack, heap) {
    if (node._visitCount + 1 === node._sources.length) {
        invalidateNode(node, stack);
    } else if (node._visitCount) {
        node._visitCount++;
    } else {
        node._visitCount++;
        heap.push(node);
    }
};
const propagate1 = function (sourceCell) {
    const heap = new Heap(cellRankComparator);
    const stack = sourceCell._sinks.slice();
    let node;
    while (true) {
        if (stack.length) {
            node = stack.pop();
            visit(node, stack, heap);
        } else if (heap.size()) {
            node = heap.pop();
            if (node._visitCount)
                invalidateNode(node, stack);
        } else {
            break;
        }
    }
};

// Propagation algorithm 2 - Using topological sort
let cachedStack;

const propagate2 = function (sourceCell) {
    const sortStack = cachedStack || [];
    sourceCell._isDirty = true;

    function visit(node) {
        if (!node._visitCount) {
            node._visitCount = 1;
            node._sinks.forEach(visit);
            sortStack.push(node);
        }
    }

    if (!cachedStack)
        visit(sourceCell);
    cachedStack = sortStack;

    while (sortStack.length) {
        const current = sortStack.pop();
        current._visitCount = 0;
        if (current._isDirty && current.invalidate()) {
            current._sinks.forEach(function (ch) { ch._isDirty = true; });
            current._isDirty = false;
        }
    }
};

let propagate = propagate1;

// Mixins
const WatchOps = {
    watch(w) {
        this._watchers.push(w);
        const self = this;
        return function unWatch() {
            const i = self._watchers.indexOf(w);
            if (i > -1)
                self._watchers.splice(i, 1);
        };
    }
};

const CommonCellOps = {
    get() {
        return this._val;
    },
    _addSink(c) {
        this._sinks.push(c);
    },
    _removeSink(c) {
        const i = this._sinks.indexOf(c);
        if (i > -1)
            this._sinks.splice(i, 1);
    },
    toString() {
        return "[object " + this.constructor.name + ": " + this._val + "]";
    }
};

function clearCell() {
    this._val = undefined;
    const self = this;
    this._sources.forEach(function (s) {
        s._removeSink(self);
    });
    this._sources.length = 0;
    this._watchers.length = 0;
    this._formula = nop;
    this._rank = -1;
}

// InputCell

const InputCell = function (val) {
    this._val = val;
    this._sinks = [];
    this._watchers = [];
    this._rank = 0;
    this._visitCount = 0;
};
Object.assign(InputCell.prototype, CommonCellOps, WatchOps);
InputCell.prototype.set = function (x) {
    const cur = this._val;
    const changed = cur !== x;
    if (changed) {
        this._val = x;
        this._watchers.forEach(function (w) {
            w(cur, x);
        });
        propagate(this);
    }
};
InputCell.prototype.update = function (f) {
    this.set(f(this._val));
};
InputCell.prototype.invalidate = function () { return true; };
InputCell.prototype._destroy = function () {
    this._val = undefined;
};

// FormulaCell
const nop = function () {console.log("NOP");};

const FormulaCell = function (sourceCells, f) {
    this._sources = sourceCells;
    this._sinks = [];
    this._watchers = [];
    this._formula = f;
    this._visitCount = 0;
    this._rank = Math.max.apply(null, sourceCells.map(function (c) {
        return c._rank + 1;
    }));
    const self = this;
    sourceCells.forEach(function (c) {
        c._addSink(self);
    });
    this.invalidate();
};
Object.assign(FormulaCell.prototype, CommonCellOps, WatchOps);
FormulaCell.prototype.invalidate = function () {
    const cur = this._val;
    const vals = this._sources.map(get);
    const x = this._formula.apply(null, vals);
    const changed = cur !== x;
    if (changed) {
        this._val = x;
        this._watchers.forEach(function (w) {
            w(cur, x);
        });
    }
    return changed;
};
FormulaCell.prototype._destroy = clearCell;

// LensCell

const LensCell = function (sourceCell, path) {
    this._sources = [sourceCell];
    this._sinks = [];
    this._watchers = [];
    this._formula = function (x) { return fp.getIn(path, x);};
    this._visitCount = 0;
    this._rank = sourceCell._rank + 1;
    this._setFn = function (x) {
        sourceCell.update(function (r) { return fp.assocIn(path, x, r);});
    };
    sourceCell._addSink(this);
    this.invalidate();
};

Object.assign(LensCell.prototype, CommonCellOps, WatchOps);
LensCell.prototype.invalidate = FormulaCell.prototype.invalidate;
LensCell.prototype.get = FormulaCell.prototype.get;
LensCell.prototype.set = function (x) {
    const changed = this._val !== x;
    this._val = x;
    if (changed)
        this._setFn(x);
    propagate(this);
};
LensCell.prototype.update = function (x) {
    const changed = this._val !== x;
    this._val = x;
    if (changed)
        this._setFn(x);
    propagate(this);
};

LensCell.prototype._destroy = function () {
    clearCell();
    this._setFn = undefined;
};

// Public
exports.cell = function cell(x) {
    return new InputCell(x);
};

exports.fcell = function fcell(cells, formula) {
    return new FormulaCell(cells, formula);
};

exports.lens = function lens(sourceCell, path) {
    return new LensCell(sourceCell, path);
};

function get(cell) {
    return cell.get();
}

exports.get = get;

exports.setPropagationAlgo = function setPropagationAlgo(type) {
    switch (type) {
        case "HEAP":
            propagate = propagate1;
            break;
        case "TOPSORT":
            propagate = propagate2;
            break;
        default:
            throw "Invalid algorithm type";
    }
};

exports.isCell = function isCell(obj) {
    return obj instanceof InputCell || obj instanceof FormulaCell || obj instanceof LensCell;
};

function safeDestroy(cell) {
    if (cell._sinks.length)
        throw Error("Can't delete cell that has dependencies. Remove dependencies first.");
    cell._destroy();
}

exports.safeDestroy = safeDestroy;

// Util

/*
Should return a single cell that changes whenever the arrayCell changes
Creates n managed cells for the items. When the list grows, new cells are created and when it shrinks,
cells are destroyed
 */
function each(arrayCell) {
    const cache = [];

    const wrapper = fcell([arrayCell], function (items) {
        const cLength = cache.length;
        items.forEach(function (item, i) {
            const prevItem = cache[i];

        });
    });
}
