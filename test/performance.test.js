const test = require("tape");
const g = require("../src");
const util = require("./testUtils");

const cell = g.cell;
const fcell = g.fcell;

function timeTest(label, f, numIterations) {
    console.log("Heap:");
    g.setPropagationAlgo("HEAP");
    console.time(label);
    util.range(numIterations).forEach(f);
    console.timeEnd(label);

    console.log("Topsort:");
    g.setPropagationAlgo("TOPSORT");
    console.time(label);
    util.range(numIterations).forEach(f);
    console.timeEnd(label);
}

function firstArg(x) {
    return x;
}

function inc(n) { return n + 1;}

test("Long chain", function (t) {
    const root = cell(-1);
    let c = root;

    util.range(1000).forEach(function () {
        c = fcell([c], inc);
    });

    t.equal(c.get(), 999);
    timeTest(t.name, function (i) { root.set(i); }, 10000);

    t.end();
});

test("Long chain, with pruning", function (t) {
    const root = g.cell(-1);
    let c = root;

    util.range(1000).forEach(function (i) {
        c = i === 500 ? fcell([c], util.constantly(0)) : fcell([c], inc);
    });

    t.equal(c.get(), 499);
    timeTest(t.name, function (i) { root.set(i); }, 10000);

    t.end();
});

test("Wide diamond", function (t) {
    const root = g.cell(0);
    const fcells = util.range(1000).map(function () {
        return fcell([root], inc);
    });
    const bottom = fcell(fcells, util.sum);

    t.equal(bottom.get(), 1000);

    timeTest(t.name, function (i) { root.set(i); }, 10000);

    t.end();
});

test("Wide diamond, double inputs", function (t) {
    const i1 = g.cell(-1);
    const i2 = g.cell(-1);
    const fcells = util.range(1000).map(function () {
        return fcell([i1, i2], function (a, b) { return a + b;});
    });
    const bottom = fcell(fcells, firstArg);

    t.equal(bottom.get(), -2);

    timeTest(t.name, function (i) { i1.set(i); }, 10000);

    t.end();
});
