const tape = require("tape");
const util = require("./testUtils");
const genshi = require("../src");
const { cell, fcell, lens, setPropagationAlgo, isCell, safeDestroy } = genshi;

function test(name, tc) {
    let algo = "HEAP";
    setPropagationAlgo(algo);
    tape(algo + ":" + name, tc);
    algo = "TOPSORT";
    tape(algo + ":" + name, tc);
}

test("input cell", function (t) {
    t.plan(3);
    const c = cell(10);
    t.equal(c.get(), 10, "Correct initial value.");
    c.set(20);
    t.equal(c.get(), 20, "Correct value after set.");
    c.update(util.square);
    t.equal(c.get(), 20 * 20, "Correct value after update.");
});

test("formula cell", function (t) {
    t.plan(4);
    const c = fcell([], util.constantly(42));
    t.equal(c.get(), 42, "Can create formula cell with no dependencies");
    t.equal(genshi.get(c), 42, "Can get with function.");
    t.throws(function () {c.set(10);}, Error, "Cannot set a formula cell.");
    t.throws(function () {c.update(util.square);}, Error, "Cannot update a formula cell.");
});

test("Basic propagation: I->A", function (t) {
    t.plan(3);
    const graph = util.makeGraph([
        ["I", [], "pre"],
        ["A", ["I"], function (v) { return v + "fix";}]
    ]);
    const I = graph["I"];
    const A = graph["A"];
    t.equal(A.get(), "prefix", "Formula cell is immediately set with correct value.");

    I.set("suf");
    t.equal(A.get(), "suffix", "Change is propagated after set.");

    I.update(function (s) {return s.toUpperCase();});
    t.equal(A.get(), "SUFfix", "Change is propagated after update.");
});

test("Single chain: I->A->B->C", function (t) {
    t.plan(2);
    const graph = util.makeGraph([
        ["I", [], 2],
        ["A", ["I"], util.square],
        ["B", ["A"], util.square],
        ["C", ["B"], util.square]
    ]);
    const state = function () { return ["I", "A", "B", "C"].map(function (n) { return graph[n].get();}); };
    t.same(state(), [2, 4, 16, 256], "Initial chain is correct.");
    graph["I"].update(util.inc);
    t.same(state(), [3, 9, 81, 81 * 81], "Change is propagated through the chain.");
});

test("Forked chain: I->(A,B)", function (t) {
    t.plan(2);
    const I = cell(50);
    const A = fcell([I], function (x) {return x / 2;});
    const B = fcell([I], function (x) {return x * 2;});
    t.same([I.get(), A.get(), B.get()], [50, 25, 100], "Initial forked values are correct.");
    I.set(100);
    t.same([I.get(), A.get(), B.get()], [100, 50, 200], "Values are propagated to all branches");
});

test("Disconnected graph: I1->A,I2->B", function (t) {
    t.plan(6);
    const I1 = cell(0);
    const I2 = cell(0);
    const A = fcell([I1], util.square);
    const B = fcell([I2], util.inc);

    t.equal(A.get(), 0, "Initial value first component is correct.");
    t.equal(B.get(), 1, "Initial value second component is correct.");
    I1.set(4);
    t.equal(A.get(), 16, "Only cell from first component is changed.");
    t.equal(B.get(), 1, "Cell from second component is unchanged.");
    I2.set(10);
    t.equal(A.get(), 16, "Cell from first component is unchanged.");
    t.equal(B.get(), 11, "Cell from second component is changed.");
});

test("Multiple sources: I1->(A,B),I2->(A,B),A->B", function (t) {
    t.plan(6);
    const I1 = cell(50);
    const I2 = cell("myValue");
    const A = fcell([I1, I2], function (a, b) { return [a, b]; });
    const B = fcell([I1, I2, A], function (a, b, c) { return [c.join("-"), a + b.length];});
    t.same(A.get(), [50, "myValue"]);
    t.same(B.get(), ["50-myValue", 57]);
    I1.update(util.square);
    t.same(A.get(), [2500, "myValue"]);
    t.same(B.get(), ["2500-myValue", 2507]);
    I2.set("newValue");
    t.same(A.get(), [2500, "newValue"]);
    t.same(B.get(), ["2500-newValue", 2508]);
});

test("Two input cells with large number of dependents: I1->(A[i]*),I2->(A[i]*),(A[i]*)->B", function (t) {
    t.plan(3);
    const gData = [["I1", [], 0], ["I2", [], 0]].concat(util.range(100).map(function (i) {
        return ["A" + i, ["I1", "I2"], util.inc];
    }));
    gData.push(["B", util.range(100).map(function (i) { return "A" + i;}), util.inc]);
    const graph = util.makeGraph(gData);
    t.equal(graph["B"].get(), 101);
    graph["I1"].set(1);
    t.equal(graph["B"].get(), 201);
    graph["I2"].set(1);
    t.equal(graph["B"].get(), 301);
});

test("Ordering: I->(A,B),(A,B)->C", function (t) {
    t.plan(5);
    const log = [];

    const I = cell(0);
    const A = fcell([I], util.withLog("A", log, util.inc));
    const B = fcell([I], util.withLog("B", log, util.inc));
    fcell([A, B], util.withLog("C", log, util.inc));
    let inv = util.invokeOrder(log);
    t.ok(util.inOrder(["A", "C"], inv));
    t.ok(util.inOrder(["B", "C"], inv));

    log.length = 0;
    I.set(1);

    const invokeCount = util.frequencies(log);
    inv = util.invokeOrder(log);
    t.same([invokeCount["A"], invokeCount["B"], invokeCount["C"]], [1, 1, 1], "Formulas are called at most once per propagation.");
    t.ok(util.inOrder(["A", "C"], inv));
    t.ok(util.inOrder(["B", "C"], inv));
});

test("Formulas are called in a topsort order: ", function (st) {
    st.test("I->A->B", function (t) {
        t.plan(1);
        const log = [];
        const graph = util.makeGraph([
            ["I", [], 0],
            ["A", ["I"], util.withLog("A", log, util.inc)],
            ["B", ["A"], util.withLog("B", log, util.inc)]
        ]);
        log.length = 0;
        graph["I"].set(1);
        const inv = util.invokeOrder(log);
        t.ok(util.inOrder(["A", "B"], inv));
    });

    st.test("I->(A,B),A->B", function (t) {
        t.plan(1);
        const log = [];
        const graph = util.makeGraph([
            ["I", [], 0],
            ["A", ["I"], util.withLog("A", log, util.inc)],
            ["B", ["I", "A"], util.withLog("B", log, util.inc)]
        ]);
        log.length = 0;
        graph["I"].set(1);
        const inv = util.invokeOrder(log);
        t.ok(util.inOrder(["A", "B"], inv));
    });

    st.test("Two inputs: I1->A,I2->A,(I1,I2,A)->B", function (t) {
        t.plan(1);
        const log = [];
        const graph = util.makeGraph([
            ["I1", [], 0],
            ["I2", [], 0],
            ["A", ["I1", "I2"], util.withLog("A", log, util.inc)],
            ["B", ["I1", "A"], util.withLog("B", log, util.inc)]
        ]);
        log.length = 0;
        graph["I1"].set(1);
        const inv = util.invokeOrder(log);
        t.same(inv, ["A", "B"], "A is invoked before B.");
    });
});

test("Unchanged cells do not propagate their changes", function (t) {
    t.test("Various branches", function (st) {
        st.plan(5);
        const log = [];

        function append(suffix) {
            return util.withLog(suffix, log, function (s) {
                return s + suffix;
            });
        }

        const root = cell("o");
        const ll1 = fcell([root], util.constantly("ll1"));
        const ll2 = fcell([ll1], append("ll2")); // behind constant cell

        const l1 = fcell([root], append("l1"));
        const l2 = fcell([l1], util.constantly("l2"));
        const r1 = fcell([root], append("r1"));
        const r2 = fcell([r1], append("r2"));
        const m = fcell([l2, r2], function (a, b) {
            return a + b; // one branch changes
        });
        const rr1 = fcell([root], append("rr1"));
        const rr2 = fcell([rr1], append("rr2"));

        st.same([ll2.get(), m.get(), rr2.get()], ["ll1ll2", "l2or1r2", "orr1rr2"]);
        let freqs = util.frequencies(log);
        st.ok(["ll2", "l1", "r1", "r2", "rr1", "rr2"].every(function (n) { return n in freqs;}), "All formulas are called initially.");
        log.length = 0;
        root.set("X");
        freqs = util.frequencies(log);
        const inv = util.invokeOrder(log);
        st.same([ll2.get(), m.get(), rr2.get()], ["ll1ll2", "l2Xr1r2", "Xrr1rr2"], "Left branch is not changed.");
        st.equal(inv.length, 5);
        st.ok(["l1", "r1", "r2", "rr1", "rr2"].every(function (n) { return n in freqs;}), "ll2 branch is not invoked.");
    });

    t.test("Fast and slow path: I->L1->A,I->R1->R2->R3->R4->A", function (st) {
        st.plan(6);
        const log = [];
        const f = util.withLog("counter", log, util.inc);
        const joinCount = util.withLog("A", log, util.inc);
        const graph = util.makeGraph([
            ["I", [], 0],
            ["L1", ["I"], f],
            ["R1", ["I"], f],
            ["R2", ["R1"], f],
            ["R3", ["R2"], f],
            ["R4", ["R3"], f],
            ["A", ["L1", "R4"], joinCount]
        ]);
        const invokeCount = util.frequencies(log);
        st.equal(graph["A"].get(), 6);
        st.equal(invokeCount["counter"], 5);
        st.equal(invokeCount["A"], 1);

        log.length = 0;
        graph["I"].set(1);
        st.equal(graph["A"].get(), 8);
        st.equal(invokeCount["counter"], 5);
        st.equal(invokeCount["A"], 1);
    });

    t.test("Fast and slow path, with stopped propagation: I->L1->A,I->R1->R2->R3->R4->A", function (st) {
        st.plan(6);
        const log = [];
        const fl1 = util.withLog("L1", log, util.inc);
        const fl2 = util.withLog("R2", log, util.constantly(97));
        const fr3 = util.withLog("R3", log, util.inc);
        const graph = util.makeGraph([
            ["I", [], 0],
            ["L1", ["I"], fl1],
            ["R1", ["I"], util.inc],
            ["R2", ["R1"], fl2],
            ["R3", ["R2"], fr3],
            ["R4", ["R3"], util.inc],
            ["A", ["L1", "R4"], util.inc]
        ]);
        let invokeCount = util.frequencies(log);
        st.equal(graph["A"].get(), 101);
        st.equal(invokeCount["L1"], 1);
        st.equal(invokeCount["R3"], 1);

        log.length = 0;
        graph["I"].set(1);
        invokeCount = util.frequencies(log);
        st.equal(graph["A"].get(), 102);
        st.equal(invokeCount["L1"], 1);
        st.notOk(invokeCount["R3"], "Cell behind constant cell should not be called.");
    });
    t.end();
});

test("Lens cell", function (t) {
    t.test("Getting nested property", function (st) {
        st.plan(3);
        const c = cell({ a: [{}, { b: "hello", c: "test" }, {}], d: 10 });
        const l = lens(c, ["a", 1, "b"]);
        st.equal(l.get(), "hello");

        l.set("bye");
        st.same(c.get(), { a: [{}, { b: "bye", c: "test" }, {}], d: 10 });
        st.equal(l.get("bye"), "bye");
    });

    t.test("Lens propagation", function (st) {
        st.plan(8);
        const c = cell([1, 2, 3]);
        const headLens = lens(c, [0]);
        const log = [];
        fcell([c], util.withLog("w", log, function (arr) { return arr[2]; }));
        st.equal(headLens.get(), 1);
        st.equal(log.length, 1, "Watcher is invoked once for initialization.");
        c.set([4, 5, 6]);
        st.equal(headLens.get(), 4, "Head is replaced.");
        st.equal(log.length, 2, "Watcher is invoked again, because parent changed.");
        headLens.set(4);
        st.equal(headLens.get(), 4, "Head is unchanged");
        st.equal(log.length, 2, "Propagation is not triggered, because new value == old value.");
        headLens.set(2);
        st.equal(headLens.get(), 2, "Changing the head by setting the lens");
        st.equal(log.length, 3, "Propagation is triggered, because root cell changed.");
    });

    t.test("Lenses are observable", function (st) {
        st.plan(4);
        const c = cell({ items: [{ id: 1, name: "A" }, { id: 2, name: "B" }] });
        const l1 = lens(c, ["items", 0]);
        const l2 = lens(l1, ["name"]);
        st.equal(l1.get().id, 1);
        st.equal(l2.get(), "A");
        c.update(function (v) {
            const itemsClone = v.items.slice();
            itemsClone[0] = { ...itemsClone[0], name: "A2" };
            return { items: itemsClone };
        });
        st.equal(l1.get().id, 1);
        st.equal(l2.get(), "A2");
    });
});

test("Watchers", function (t) {
    const c = cell(10);
    const f = fcell([c], util.square);
    const log = [];
    const w1 = c.watch(function (oldVal, newVal) {
        log.push([oldVal, newVal]);
    });
    const w2 = f.watch(function (oldVal, newVal) {
        log.push([oldVal, newVal]);
    });
    t.same(log, [], "Watchers are only fired on changes.");
    c.set(20);
    t.same(log, [[10, 20], [100, 400]], "Both watchers get fired on update.");
    log.length = 0;
    c.set(-20);
    t.same(log, [[20, -20]], "Only first wachter sees change.");
    log.length = 0;
    w1();
    c.set(0);
    t.same(log, [[400, 0]], "First watcher is removed.");
    log.length = 0;
    w2();
    t.same(log, [], "Watchers have been removed.");
    t.end();
});

test("Clearing cells", function (t) {
    let c = cell(10);
    safeDestroy(c);
    t.equal(c.get(), undefined);

    c = cell(10);
    let f = fcell([c], util.inc);
    let f2 = fcell([c], util.square);
    t.throws(function () {safeDestroy(c);}, Error, "Can't clear cell with dependencies.");
    t.same([c.get(), f.get(), f2.get()], [10, 11, 100], "Cells are still intact.");
    safeDestroy(f);
    t.equal(f.get(), undefined, "Formula cell without dependencies can be cleared.");
    t.equal(c.get(), 10, "Input cell is still intact");
    c.set(50);
    t.equal(c.get(), 50, "Input cell is updated.");
    t.equal(f.get(), undefined, "Formula cell is disconnected");
    t.equal(f2.get(), 2500, "Other dependent cells are unaffected");

    safeDestroy(f2);
    safeDestroy(c);
    t.equal(c.get(), undefined, "Input cell is cleared.");
    c.set(0);

    t.end();
});

test("Cell meta", function (t) {
    t.equal(cell("test").toString(), "[object InputCell: test]");
    let c = cell({ myKey: [1, "two", true] });
    const f = fcell([c], function (o) { return o.myKey[1]; });
    const l = lens(c, ["myKey", 0]);
    t.equal(c.toString(), "[object InputCell: [object Object]]");
    t.equal(f.toString(), "[object FormulaCell: two]");
    t.equal(l.toString(), "[object LensCell: 1]");

    t.ok(isCell(c));
    t.ok(isCell(f));
    t.ok(isCell(l));
    c = cell(new Date());
    t.equal(typeof c, "object");
    t.end();
});
