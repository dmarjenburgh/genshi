const test = require("tape");
const Heap = require("../src/heap");

test("Heap", function (t) {
    t.test("Adding and removing an item", function (st) {
        st.plan(6);
        const h = new Heap();
        st.equal(h.size(), 0);
        st.equal(h.push(1), 1);
        st.equal(h.pop(), 1);
        st.equal(h.size(), 0);
        st.equal(h.pop(), undefined);
        st.equal(h.size(), 0);
    });

    t.test("Adding items in increasing order", function (st) {
        st.plan(4);
        const h = new Heap();
        const pushResults = [1, 2, 3, 4, 5].map(h.push.bind(h));
        st.same(pushResults, [1, 2, 3, 4, 5], "Push returns number of items on the heap.");
        st.equal(h.size(), 5);
        const out = [h.pop(), h.pop(), h.pop(), h.pop(), h.pop()];
        st.same(out, [1, 2, 3, 4, 5], "Pop returns items in correct order.");
        st.equal(h.size(), 0);
    });

    t.test("Adding items in reverse order", function (st) {
        st.plan(2);
        const h = new Heap();
        [5, 4, 3, 2, 1].forEach(function (x) {
            h.push(x);
        });
        const out = [h.pop(), h.pop(), h.pop(), h.pop(), h.pop()];
        st.same(out, [1, 2, 3, 4, 5]);
        st.equal(h.size(), 0);
    });

    t.test("Custom comparator", function (st) {
        st.plan(3);
        const h = new Heap(function (a, b) {
            return a.value - b.value;
        });
        h.push({value: 10, name: "Alice"});
        h.push({value: 30, name: "Bob"});
        h.push({value: 20, name: "Charlie"});
        st.equal(h.pop().name, "Alice");
        st.equal(h.pop().name, "Charlie");
        st.equal(h.pop().name, "Bob");
    });
    t.end();
});
