const g = require("../src");

module.exports.square = function square(x) {return x * x;};

module.exports.constantly = function constantly(v) {return function () {return v;};};

module.exports.sum = function sum() {
    let total = 0;
    for (let i = 0; i < arguments.length; i++)
        total += arguments[i];
    return total;
};

module.exports.inc = function inc() {
    let total = 1;
    for (let i = 0; i < arguments.length; i++)
        total += arguments[i];
    return total;
};

module.exports.withLog = function withLog(name, log, f) {
    return function () {
        const args = Array.prototype.slice.call(arguments);
        log.push([name, args]);
        return f.apply(null, args);
    };
};

module.exports.frequencies = function frequencies(log) {
    const acc = {};
    log.forEach(function (x) {
        if (!(x[0] in acc))
            acc[x[0]] = 0;
        acc[x[0]]++;
    });
    return acc;
};

module.exports.invokeOrder = function invokeOrder(log) {
    return log.map(function (x) {
        return x[0];
    });
};

module.exports.makeGraph = function makeGraph(nodes) {
    const m = {};
    nodes.forEach(function (node) {
        const n = node[0];
        const parents = node[1];
        const f = node[2];
        if (!parents.length) {
            m[n] = g.cell(f);
        } else {
            m[n] = g.fcell(parents.map(function (p) { return m[p]; }), f);
        }
        m[n].name = n;
    });
    return m;
};

module.exports.range = function range(n) {
    return Array.apply(null, Array(n)).map(function (_, i) { return i;});
};

module.exports.inOrder = function inOrder(elems, arr) {
    const idxs = elems.map(function (x) { return arr.indexOf(x); });
    if (idxs.indexOf(-1) > -1)
        throw "Element is not in array";
    for (let i = 0; i < idxs.length - 1; i++)
        if (idxs[i] > idxs[i + 1])
            return false;
    return true;
};
