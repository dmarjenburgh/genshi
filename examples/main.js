import genshi from "../";

const cell = genshi.cell;
const fcell = genshi.fcell;

const myCell = cell(10);
myCell.set(20);
myCell.update(function (x) {
    return x * 2;
});
const mCell = fcell([myCell], function (x) { return Math.floor(x / 5)*5;});
fcell([mCell], console.log);
for (let i = 0; i <= 50; i++) {
    myCell.set(i);
}
