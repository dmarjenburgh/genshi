# Things to vary

- Depth first vs breadth first
- Math.floor vs Math.truncate
- ES6 map vs _visitCount property on cells
- ES6 classes vs function prototypes
- Different bundlers browserify/webpack/rollup/closure or single file
- Setting array item to undefined/null vs deleting